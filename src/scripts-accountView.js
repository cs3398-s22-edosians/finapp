// 0 = expense
// 1 = income
var type = 0;
var balance = 0;

function openModal() {
    let modal = document.getElementById("modal");
    modal.style.display = "block";
}

function closeModal() {
    let modal = document.getElementById("modal");
    modal.style.display = "none";
    document.getElementById("amount").value = "";
    document.getElementById("from").value = "";
    document.getElementById("date").value = "";
    if(type === 1) {
        changeType();
    }
}

function changeType() {
    let typeBtn = document.getElementById("type");
    if(type === 0) {
        type = 1;
        typeBtn.innerHTML = "Income";
    } else {
        type = 0;
        typeBtn.innerHTML = "Expense";
    }
}

function updateBalance() {
    let amount = parseFloat(document.getElementById("amount").value);
    let date = document.getElementById("date").value;
    let from = document.getElementById("from").value;
    let balanceField = document.getElementById("balanceAmt");

    if(type === 0) {
        balance = balance - amount;
    } else {
        balance = balance + amount;
    }

    let balanceFormatter = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD', minimumFractionDigits: 2});
    balanceField.innerHTML = balanceFormatter.format(balance);

    let table = document.getElementById("tlTable");
    let row = table.insertRow(-1);
    row.insertCell().innerHTML = date;
    if(type === 0) {
        row.insertCell().innerHTML = "Expense";
    } else {
        row.insertCell().innerHTML = "Income";
    }
    row.insertCell().innerHTML = from;
    cell = row.insertCell();
    cell.innerHTML = balanceFormatter.format(amount);
    cell.classList.add('amount');
    if(type === 0) {
        cell.classList.add('negAmount');
    }
    closeModal();
}