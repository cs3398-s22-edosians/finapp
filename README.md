# Finapp
> Finance app to help users manage income and expenses
<!-- removed for now >> Live demo [_here_](https://www.example.com). -->

<!-- If you have the project hosted somewhere, include the link here. -->

![image](https://cdn-icons-png.flaticon.com/256/1211/1211547.png)

## Table of Contents

* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
<!-- * [Screenshots](#screenshots) -->
<!-- * [Setup](#setup) -->
<!-- * [Usage](#usage) -->
* [Project Status](#project-status)
<!-- * [Room for Improvement](#room-for-improvement) -->
* [Acknowledgements](#acknowledgements)
<!-- * [License](#license) -->


## General Information
<!-- Provide general information about your project here. -->

- Team Members: Husain Alshaikhahmed, Lee Balboa, Roldan Cortes, Andew Killian

- We are creating a banking desktop application that guide our users on how to develop healthy financial habits. We provide our users with visual information about their spending habits, monthly recurring charges. 

- Our application is aimed towards individuals who struggle to save money. Whether it's due to too many recurring monthly charges, unhealthy spending habits or just looking for a way to make better financial decision. 
 
-  With current real estate prices, inflation, and other economic issues, saving money can be a real challenge, especially if household income is low. We hope to impact the lives of our users by saving our users money over time.



## Technologies Used

<!-- - [Java](https://www.java.com/)
- [JavaFX](https://openjfx.io/)
- [MySQL](https://www.mysql.com/)
--- -->
- HTML5, CSS3, JavaScript
 
- [React](https://reactjs.org/) 17.0.2

- [Electron](https://www.electronjs.org/) 17.0.1


## Features

Sprint 1 Features:

- Basic income & expense tracking

    - User Story: I, Lee, as a student and consumer, would like the ability to enter my expenses and income in this app so that I can keep track of my running balance. 

- Budget setting feature

    - User Story: I, Andrew, as a student of Texas State University would like to be able to create a banking app that also allows users to set a budget for themselves. 
 


<!-- removed for now ## Screenshots
![Example screenshot](./img/screenshot.png) -->
<!-- If you have screenshots you'd like to share, include them here. -->


<!-- removed for now ## Setup
What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

Proceed to describe how to install / setup one's local environment / get started with the project. -->


<!-- removed for now ## Usage
How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here` -->


## Project Status

Project is: _in progress_


<!-- ## Room for Improvement
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

Room for improvement:
- Improvement to be done 1
- Improvement to be done 2

To do:
- Feature to be added 1
- Feature to be added 2 -->


## Acknowledgements

- Thanks to Dr. Ted Lehr, our CS3398 professor


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->

## Sprint 1

## Andrew Killian

* Jira Task: Form Research
* BitBucket URL: https://bitbucket.org/cs3398-s22-edosians/finapp/commits/402145418829f4859d4bd9ca057a265dcf3dba2b
* What it is: Research on how to implement HTML web forms and additional best practices to use for forms made in next sprints  
<br />

* Jira Task: Create Log in Form
* BitBucket URL: https://bitbucket.org/cs3398-s22-edosians/finapp/commits/branch/log_in_form 
* Where it's used: This is the inital log in form a user sees when they go to our website  
<br />

* Jira Task: Create Welcome/Home Page
* Bitbucket URL: https://bitbucket.org/cs3398-s22-edosians/finapp/commits/branch/welcome_page
* Where it's used: This is the inital welcome page that a user sees after sucessfully loggin in  
<br />

* Jira Task: Research Authentication
* Bitbucket URL: https://bitbucket.org/cs3398-s22-edosians/finapp/commits/20f3c00cf75ae0ec0fef757143941729a289ef7a
* What it is: Research on best practices for user / password / login authentication  
<br />

* Jira Task: Demo Log In
* Bitbucket URL: https://bitbucket.org/cs3398-s22-edosians/finapp/commits/branch/demo_log_in
* Where it's used: This is an aside login on the inital login form that allows you to bypass having to type in a username and password to tour the site  
<br />

- Next Steps:  
* Create a form that allows a user to enter a certain dollar amount budget they want to stay under in certain spending categories
* Implement user log in authentication




## Retrospective

## Edossians

## What Went Well?

- We didn’t kill each other.  
- We didn’t die.  
- Helping/teaching other how to create branches, and merge them into master  
- Showing up to zoom meetings when we say we’re going to. 
- Andrew – Designing and coding the initial log in form/page, as well as teaching my team members how to create branches, check them out, and merge them into master  
- Roldan - 

## What Can I do Better?

## Andrew

- Time management. I found that this sprint I vastly underestimated how much time certain tasks were going to take. I can try to be more accurate and give myself more time to complete a task. 
- In addition, I will no longer push trivial commits where I only changed one line of code. 
- Lastly, I can definitely be better at making commits or completing tasks on a more frequent basis as opposed to doing it all at once towards the end of the sprint.  
- My team and instructor will know that I am doing better by more communication on the slack channel and by not waiting too long into the sprint to start working on tasks and committing.  
<br />
## Roldan

## What Might Be Impeding Us from Performing Better?
 
- Communication (Being on the same page about what each one of us is working on in the application)
- Procrastinating (Waiting much too long into the sprint to start working on our tasks)


# Sprint 3

## Team Contributions

- Lee Balboa
    - [FIN-50](https://cs3398s22edosians.atlassian.net/browse/FIN-50) Fixed balance calculation and CSS
        - Branch: [feature/FIN-50-balance-display](https://bitbucket.org/cs3398-s22-edosians/%7B840f1bbc-d702-4df9-b4ed-4142aba5eb71%7D/branch/feature/FIN-50-balance-display)
        - In sprint 2, there was a bug with the balance calculation (parsed integers instead of floats), and the CSS was bad. I fixed those issues with this task.
    - [FIN-55](https://cs3398s22edosians.atlassian.net/browse/FIN-55)
        - Branch: I made a mistake when implementing this feature and had the FIN-50 branch checked out still. I did make a branch for this but it went unused.
        - This implemented the functionality for the transaction list, so when you enter a transaction, it is recorded in the list properly. This is necessary for the project so users can keep track of their previous transactions.

## Future Steps

- Lee: Since there's no future sprint, if we were to continue this project, the main next step would be to create a backend and databse to make a proper application. This was too difficult for us during this semester, but future work would require a proper stack.

## Retrospective

- What went well, what didn't go well?
    - (Team) We accomplished a good amount of work, despite setbacks.
    - (Team) We both struggled with figuring out how to accomplish the main goal for each of us, which meant we spent a lot of time not getting work done.
    - (Lee) The main struggle for me is that I couldn't find a good way of doing simple data storage without making a proper database solution, so I had to drop those tasks.

- What might be impeding us from performing better?
    - (Team) We both don't have a lot of web development experience, so we were having to spend a lot of time figuring out the basics and doing research. Then, if something didn't work out, we had to drastically change directions, costing time.

- What can I do to improve?
    - (Lee) If there was a future sprint, I would try to "fail faster", and figure out if a plan is going to work or not work in the first few days of the sprint. By spending a lot of time looking into details, it makes it harder to get a product out the door by the end of the sprint. This could be measured by me making a "figure out if X works" task in Jira, then completing it by the first couple days of the sprint.



