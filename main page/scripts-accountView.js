function openModal() {
    let modal = document.getElementById("modal");
    modal.style.display = "block";
}

function closeModal() {
    let modal = document.getElementById("modal");
    modal.style.display = "none";
}

// 0 = expense
// 1 = income
var type = 0;

function changeType() {
    let typeBtn = document.getElementById("type");
    if(type === 0) {
        type = 1;
        typeBtn.innerHTML = "Income";
    } else {
        type = 0;
        typeBtn.innerHTML = "Expense";
    }
}

var balance = 0;

function updateBalance() {
    let amount = parseInt(document.getElementById("amount").value);
    let balanceField = document.getElementById("balanceAmt");

    if(type === 0) {
        balance = balance - amount;
    } else {
        balance = balance + amount;
    }

    let balanceFormatter = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD', minimumFractionDigits: 2});
    balanceField.innerHTML = balanceFormatter.format(balance);
    closeModal();
}