## Project Plan
### Transactions
- Transactions are entered manually
- Transaction types:
	- Payments
		- Subscriptions 
		- Bills
	- Income
		- Paycheck
		- Check
		- Direct Deposit
- Fields:
	- Amount
	- Date
	- Person who is paid
	- Tags (not a priority)
	- Description

### Bank Accounts
- Account Number
- Account Name
- Initial Balance
- Default account flag

### Login
- Only basic functionality for now
- Remember me function

### UI/Display
- Graphs/pie charts for categories
- Login page
- About page
- Home page
	- pre-login page with a summary of the product
- Acccounts page/main page (for each account) (Roldan's tasks)
	- balances for each bank account
	- graphs
	- transaction list summary
	- account switcher
	- transaction form (opened by a button)
- Transaction list/history page (for each account) (Lee's task)


### Technical Details
- JSON file stored locally
- Electron app
	- Roldan doing research
- Potentially using StripeJS to handle transactions
- Potentially using React for UI

